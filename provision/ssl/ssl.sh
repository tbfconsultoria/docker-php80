#!/bin/bash
echo ""
echo "Exemplo: mydomain.local"
echo ""
while true; do
    read -p 'INFORME O DOMINIO: ' domain
    if [ -z "$domain" ]; then
        echo "[!] Informe um dominio valido"
    else
        break
    fi
done

#BASEDIR=$(dirname "$0")
DOMAIN_KEY="/etc/apache2/ssl/$domain.key"
DOMAIN_PEM="/etc/apache2/ssl/$domain.pem"
ROOT_CA="/root/ssl/rootCA.crt"

mkcert -key-file $DOMAIN_KEY -cert-file $DOMAIN_PEM $domain

if [ $? -eq 0 ]; then
cp $(mkcert -CAROOT)/rootCA.pem $ROOT_CA

chmod 0644 $DOMAIN_KEY $DOMAIN_PEM

echo "SUCESSO"
echo "-----------------------"
echo "> Add no VirtualHost:"
echo ""
echo "      <VirtualHost *:443>"
echo "          ServerName $domain"
echo "          # .."
echo ""
echo "          SSLEngine On"
echo "          SSLCertificateFile $DOMAIN_PEM"
echo "          SSLCertificateKeyFile $DOMAIN_KEY"
echo ""
echo "          # .."
echo "      </VirtualHost>"
echo ""
echo "OPCIONAL"
echo "-----------------------"
echo "> Se quiser remove o alerta do browser que o site não é seguro"
echo "> instale o certificado \"provision/ssl/rootCA.crt\" no seu browser"
echo "> depois reinicie o browser."
echo ""
echo "      Chrome: execute na url \"chrome://restart\""
echo "      Firefox: execute na url \"about:profiles\" e click no botão Reiniciar"
echo ""
fi