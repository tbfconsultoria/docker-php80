# Docker

> - Quando a data/hora dos containers não estiver correta, execute: `$ wsl --shutdown`

Este docker possui:

- SSL
- PHP 8.0
- MySQL latest
- APACHE 2.4
- Composer
- MailHog
- [WP-CLI](https://make.wordpress.org/cli/handbook/guides/quick-start/)

Com a estrutura de pasta

| Pasta      | Descrição                            |
| ---------- | ------------------------------------ |
| provision/ | Contendo os arquivos de configuração |
| www        | Pasta das aplicações                 |

### SSL

---

Para usar SSL execute:

```shell
$ docker exec -it -u root php80 bash /root/ssl/ssl.sh
```

### MySQL

---

> Nos CLIENT o **HOST** use como `localhost` ou `127.0.0.1`  
> Client recomendado [DBeaver Community](https://dbeaver.io/download/)

Dados para configurar conexão nas aplicações PHP:

```plain
Host: php80_mysql
Port: 3306
User: root
Pass: root
```

### SMTP

---

> Dashboard [http://localhost:8025](http://localhost:8025)

Dados de conexão:

```plain
Host: localhost
Port: 25
User: null
Pass: null
Auth: false
```

### WordPress

---

Como baixar e instalar o wordpress via WP-CLI

```shell
# Acesse o bash do container
$ docker exec -it php80 bash

# Baixa o core do wordpress
$ wp core download --locale=pt_BR --path=/var/www/exemple

# Cria o wp-config.php
$ cd /exemple
$ wp config create --dbuser=root --dbpass=root --dbhost=php80_mysql:3306 --dbname=exemple

# Se for customizar o prefixo das tabelas o momento é agora!
# edite o arquivo "wp-config.php"

# Cria o banco de dados
$ wp db create

# Por fim, acesse a url do site e termine de configurar
```

### Root

---

Para acessar o container como `root` execute:

```shell
$ docker exec -it -u root php80 bash
```
