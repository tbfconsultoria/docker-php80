FROM php:8.0-apache

RUN apt-get update; apt-get install -y --no-install-recommends \
    nano \
    cron \
    wget \
    libzip-dev \
    libxml2-dev \
    libgmp-dev 

RUN docker-php-ext-install pdo_mysql mysqli bcmath exif soap zip gmp intl
RUN docker-php-ext-enable opcache

# LOCALE
#------------
RUN apt-get update; apt-get install -y --no-install-recommends locales
RUN sed -i 's/# pt_BR.UTF-8 UTF-8/pt_BR.UTF-8 UTF-8/g' /etc/locale.gen
RUN echo "LANG=pt_BR.UTF-8" >> /etc/default/locale
RUN locale-gen
RUN rm -rf /etc/localtime && ln -s /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime
ENV LC_ALL pt_BR.UTF-8
ENV LANG pt_BR.UTF-8
ENV LANGUAGE pt_BR.UTF-8

# GD
#------------
RUN apt-get update; apt-get install -y --no-install-recommends \
    libfreetype6-dev libjpeg-dev libwebp-dev libpng-dev
RUN docker-php-ext-configure gd --with-freetype=/usr --with-jpeg=/usr --with-webp=/usr
RUN docker-php-ext-install gd

# IMAGICK
#------------
RUN apt-get update; apt-get install -y --no-install-recommends \
    libmagickwand-dev libmagickcore-dev && pecl install imagick 
RUN docker-php-ext-enable imagick

# COMPOSER
#------------
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# WP-CLI
#------------
RUN apt-get update; apt-get install -y --no-install-recommends default-mysql-client less
RUN curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
RUN mv wp-cli.phar /usr/local/bin/wp && chmod +x /usr/local/bin/wp

# SSL
#------------
RUN curl -JLO https://dl.filippo.io/mkcert/latest?for=linux/amd64
RUN chmod +x mkcert-v*-linux-amd64
RUN mv mkcert-v*-linux-amd64 /usr/local/bin/mkcert
RUN mkdir /home/.ssl && chmod 0755 /home/.ssl

WORKDIR /var/www
RUN apt-get update; apt-get clean; apt-get autoclean; apt-get autoremove
RUN a2enmod rewrite ssl
RUN service apache2 restart
RUN service cron restart
USER www-data